class RPNCalculator
  # TODO: your code goes here!

  def initialize(calculator=[])
    @calculator = calculator
  end

  def push(element)
    @calculator << element
  end

  def plus
    raise("calculator is empty") if @calculator.empty?
    do_operation(:+)
  end

  def minus
    raise("calculator is empty") if @calculator.empty?
    do_operation(:-)
  end

  def times
    raise("calculator is empty") if @calculator.empty?
    do_operation(:*)
  end

  def divide
    raise("calculator is empty") if @calculator.empty?
    do_operation(:/)
  end

  def value
    @calculator[-1]
  end

  def tokens(string)
    operators = %w(+ - * /)
    string.split.map { |el| operators.include?(el) ? el.to_sym : el.to_i }
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.is_a? Numeric
        push(el)
      else
        case el
        when :+ then do_operation(:+)
        when :- then do_operation(:-)
        when :* then do_operation(:*)
        when :/ then do_operation(:/)
        end
      end
    end
    value
  end

  private

  def do_operation(op)
    calculated = [@calculator[-2].to_f, @calculator[-1].to_f].reduce(op)
    @calculator.pop(2)
    @calculator.push(calculated)
  end

end
